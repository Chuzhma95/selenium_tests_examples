class MyCustomIterator:

    def __init__(self, sequence, start_index, end_index):
        self.__sequence = sequence
        self.__end_index = end_index
        self.__current = start_index

    def __iter__(self):
        return self

    def __next__(self):
        if self.__current < len(self.__sequence) and self.__current <= self.__end_index:
            item = self.__sequence[self.__current]
            self.__current += 1
            return item
        else:
            raise StopIteration


if __name__ == '__main__':
    my_custome_iterator = MyCustomIterator([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 2, 7)
    for number in my_custome_iterator:
        print(number)