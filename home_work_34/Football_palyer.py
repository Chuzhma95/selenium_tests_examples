class FootballPlayer:
    def __init__(self, name: str, age: int, team: str):
        self.__name = name
        self.__age = age
        self.__team = team

    def __str__(self):
        return f"{self.__class__.__name__}: {{\n\tname:{self.__name}\n\tage: {self.__age}\n\tteam: {self.__team}\n}}"


if __name__ == '__main__':
    football_player = FootballPlayer('Zinchenko', 26, 'Arsenal')
    print(football_player)

