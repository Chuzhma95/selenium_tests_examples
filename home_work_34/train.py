class Wagon:
    def __init__(self):
        self.number = None

    def __str__(self):
        return f"{self.__class__.__name__}: [{self.number}]"


class TrainCar(Wagon):
    def __init__(self):
        super().__init__()
        self.number_of_passenger = 0

    def add_passenger(self, count_passenger):
        self.number_of_passenger += count_passenger

    def remove_passenger(self, count_passenger):
        self.number_of_passenger -= count_passenger

    def __len__(self):
        return self.number_of_passenger


class Train:
    def __init__(self):
        self.list_of_wagons = []

    def add_wagon(self, wagon: Wagon):
        self.list_of_wagons.append(wagon)
        wagon.number = len(self.list_of_wagons)

    def __len__(self):
        return len(self.list_of_wagons)


if __name__ == '__main__':
    train = Train()
    wagon1 = TrainCar()
    wagon2 = TrainCar()
    wagon3 = TrainCar()

    train.add_wagon(wagon1)
    train.add_wagon(wagon2)
    train.add_wagon(wagon3)

    wagon1.add_passenger(10)
    wagon2.add_passenger(20)
    wagon3.add_passenger(30)

    print(len(wagon1))
    print(len(wagon2))
    print(len(wagon3))

    print(wagon1)
    print(wagon2)
    print(wagon3)

    #print(len(train))