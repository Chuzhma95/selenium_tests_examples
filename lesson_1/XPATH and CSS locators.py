'''
site: https://www.work.ua/

XPATH:
1. //a[@class = 'btn btn-default hidden-xs' and @id='loginIcon']
2. //a[@href = '/' and @id = 'logo']
3. //span[contains(@class,'sr-')]
4. //a[@id = 'loginButton']
5. //div[@class = 'card ']/div
6. //*[@class = 'row'][2]
7. //*[@class = 'container']/div[@class ='row'][1]
8. //div[contains(@class,'link')]/a[@href='/employer/']
9. //input[@name="nearHomeCityId"]/ancestor::div
10. //p[@class="pull-left hidden-xs text-opacity add-top-sm"]/child::a[3]
11. //a[@href = '/employer/']
12. //a[@href = '/employer/' and @class="pull-right text-opacity"]/following::*
13. //*[@id = 'sm-but']
14. //input[@id = 'search']/preceding::*
15. //*[@class="btn btn-link"]/preceding-sibling::*
16. //*[contains(text(), 'Шукайте роботу')]
17. //*[contains(text(), 'Робота для')]
18. //h1[(text()='Сайт пошуку роботи №1 в Україні')]
19. //a[@href = '/jobs/by-region/']
20. //p[@class="pull-right add-top-exception"]//span
21. //*[text()='Розмістіть резюме']
22. //li/a[@href = '/articles/work-in-team/1770/']
23. //b[text() = '15 000']
24. //a[@id="dropdownMenu100"]
25. //a[@class="border-dashed-white text-opacity setsearch" and text() = 'Укрпошта']
26. //*[@class="item-logo-img e21751"]
27. //*[@class="btn-apple-store pull-left"]
28. //*[@class="btn-google-play pull-left"]
29. //*[@href="https://www.reactor.com.ua/"]
30 //*[@class="text-muted" and contains(text(),'Новини')]

CSS:
1. [id='dropdownMenu100-2']
2. div[class="text-opacity"]
3. div[class~="pull-left"]
4. div[id^="res"]
5. div[class$="-city"]
6. a[class ="text-muted hovered"]
7. a[href="/jobs/by-region/"]
8. li[class="i-by-company visible-xs-block"]
9. div[class="item-logo-img e1372630"]
10. div[class="icon-item icon-post-resume"]
11. a[id="decorMobileAppOnMain"]
12. a[id="decorHowToFindJobOnMain"]
13. a[href="/articles/resume/1367/"]
14. a[href="/jobseeker/resources/"]
15. a[id="loginIcon"]

'''
