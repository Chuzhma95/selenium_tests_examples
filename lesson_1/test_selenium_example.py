import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By


# def test_01(): # залишив для себе
#     drive_chrome = Chrome('chromedriver.exe')
#     drive_chrome.maximize_window()
#     drive_chrome.get('https://www.ukr.net')
#     actual_title = drive_chrome.title
#     expected_title = 'UKR.NET: Всі новини України, останні новини дня в Україні та Світі'
#     assert actual_title == expected_title
#     drive_chrome.quit()


def test_login():
    drive_chrome = Chrome('chromedriver.exe')
    drive_chrome.maximize_window()
    drive_chrome.get('https://www.saucedemo.com')

    login = 'standard_user'
    password = 'secret_sauce'

    # Enter login
    email_input_locator = '//*[@id="user-name"]'
    time.sleep(4)
    email_input_element = drive_chrome.find_element(By.XPATH, email_input_locator)
    email_input_element.click()
    time.sleep(4)
    email_input_element.send_keys(login)

    #Enter password
    password_input_css_locator = 'input[id="password"]'
    password_input_element = drive_chrome.find_element(By.CSS_SELECTOR, password_input_css_locator)
    password_input_element.clear()
    password_input_element.send_keys(password)
    time.sleep(2)

    #Login
    login_button_selector = 'input[id="login-button"]'
    login_button_element = drive_chrome.find_element(By.CSS_SELECTOR, login_button_selector)
    login_button_element.click()
    time.sleep(2)



