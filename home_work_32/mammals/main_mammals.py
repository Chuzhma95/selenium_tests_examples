from home_work_32.Animals import Animals
from abc import ABC, abstractmethod


class Mammals(Animals, ABC):
    def __init__(self, kind, gender, age, living_enviroment: str, eat: str):
        super().__init__(kind, gender, age)
        self.living_enviroment = living_enviroment
        self.eat = eat

    def get_milk(self):
        print('I can give milk for my children')

    @abstractmethod
    def to_eat(self):
        pass

    def data_mammals(self):
        print(f'live {self.living_enviroment}')

