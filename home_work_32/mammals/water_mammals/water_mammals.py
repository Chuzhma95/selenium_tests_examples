from home_work_32.mammals.main_mammals import Mammals


class Water(Mammals):
    def __init__(self, kind, gender, age, living_enviroment: str, eat: str, count_fin: int):
        super().__init__(kind, gender, age, living_enviroment, eat)
        self.count_legs = count_fin

    def to_swim(self):
        print('I can swim')

    def to_eat(self):
        print(f'I am eating {self.eat}')


if __name__ == '__main__':
    dolphin = Water(kind='delphinidae', gender='female', age=10, living_enviroment='sea or ocean', eat='plancton', count_fin=4)
    dolphin.to_swim()
    dolphin.data_animals()
    dolphin.data_mammals()
    dolphin.to_eat()