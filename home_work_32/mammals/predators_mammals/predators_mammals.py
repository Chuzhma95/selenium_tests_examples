from home_work_32.mammals.main_mammals import Mammals


class Predators(Mammals):
    def __init__(self, kind, gender, age, living_enviroment: str, eat: str, count_legs: int):
        super().__init__(kind, gender, age, living_enviroment, eat)
        self.count_legs = count_legs

    def to_run(self):
        print('I can run')

    def to_eat(self):
        print(f'I am eating {self.eat}')


if __name__ == '__main__':
    wolf = Predators(kind='dogs', gender='female', age=7, living_enviroment='forest', eat='rebbit', count_legs=4)
    wolf.to_run()
    wolf.data_animals()
    wolf.data_mammals()
    wolf.to_eat()