from home_work_32.mammals.main_mammals import Mammals


class Flaying(Mammals):
    def __init__(self, kind, gender, age, living_enviroment: str, eat: str, wing_lenght: int):
        super().__init__(kind, gender, age, living_enviroment, eat)
        self.wing_lenght = wing_lenght

    def to_fly(self):
        print('I can fly')

    def to_eat(self):
        print(f'I am eating {self.eat}')


if __name__ == '__main__':
    bat = Flaying(kind='hymenoptera', gender='male', age=12, living_enviroment='tree', eat='worms', wing_lenght=10)
    bat.to_fly()
    bat.data_animals()
    bat.data_mammals()
    bat.to_eat()

