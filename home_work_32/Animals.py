class Animals:
    def __init__(self, kind: str, gender: str, age: int):
        self.kind = kind
        self.gender = gender
        self.age = age

    def to_sleep(self):
        print(f'I can sleep')
        return self

    def to_poo(self):
        print(f'I can poo')
        return self

    def to_give_birth(self):
        print(f'I can birth')
        return self

    def data_animals(self):
        print(f'I {self.kind}, {self.gender}, {self.age}')


