from home_work_31.task_3.abstract_reader import Reader
from home_work_31.task_3.txt_reader import TxtReader
from home_work_31.task_3.txt_writer import TxtWriter


class TxtProxyReaderWriter(Reader):
    def __init__(self, txt_reader: TxtReader, txt_writer:TxtWriter):
        self.result = ''
        self.is_actual = False
        self.reader = txt_reader
        self.writer = txt_writer

    def read_file(self):
        if self.is_actual:
            return self.result
        else:
            self.result = self.reader.read_file()
            self.is_actual = True
            return self.result

    def write_file(self, new_data):
        self.writer.write_file(new_data)
        self.is_actual = False

    def update_actual_status(self, status):
        self.is_actual = status


if __name__ == '__main__':
    txt_reader = TxtReader('user.txt')
    txt_writer = TxtWriter ('user.txt')
    proxy_reader = TxtProxyReaderWriter(txt_reader,txt_writer)
    print(proxy_reader.is_actual, proxy_reader.read_file(), proxy_reader.is_actual)
    print(proxy_reader.is_actual, proxy_reader.read_file(), proxy_reader.is_actual)
    proxy_reader.write_file("Petro")
    print(proxy_reader.is_actual, proxy_reader.read_file(), proxy_reader.is_actual)
    print(proxy_reader.is_actual, proxy_reader.read_file(), proxy_reader.is_actual)
