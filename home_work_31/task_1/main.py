import string

alphabet = list(string.ascii_lowercase)
file = open('text.txt', 'r')
text = file.read().lower()
for i in alphabet:
    word_count = text.count(i)
    print(i, word_count)
