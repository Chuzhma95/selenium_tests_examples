from home_work_33.ship_impl.swimmable_interface import ISwimmable
from home_work_33.ship_impl.transport_interface import ITransport

# Multi Inberitance
# Abstraction
class Ship(ISwimmable, ITransport):
    def __init__(self):
        # Hiding
        self.__number_of_deck = 2
        self.__number_of_engine = 5
        self.__star_engine = False
        self.__latitude = 46.2
        self.__longitude = 30.1
        self.__depth = 0
        self.__health = 100
        self.__is_on_dock = True

    #Encapsulation
    # Polimorphism
    def swim(self, latitude, longitude):
        print('I am starting to swimming')
        self._move(latitude, longitude)
        self.__health -= 10
        print('I had already finished to swim')

    # Encapsulation
    # Polimorphism
    def _dock_off(self):
        self._start_engine()
        print('Dock off')
        self.__depth = 50
        self.__is_on_dock = False

    # Encapsulation
    # Polimorphism
    def _dock_on(self):
        print('Dock on')
        self.__depth = '10'
        self._stop_engine()
        self.__is_on_dock = True

    # Encapsulation
    # Polimorphism
    def _start_engine(self):
        print('Start Engine')
        self.__star_engine = True

    # Encapsulation
    # Polimorphism
    def _move(self, latitude, longitude):
        self._dock_off()
        print(f'Move to longitude: {longitude}, latitude: {latitude}')
        while abs(self.__latitude - latitude) < 0.2 and abs(self.__longitude - longitude) < 0.2:
            self.__latitude = self.__latitude + 0.1 if self.__latitude < latitude else self.__latitude - 0.1
            self.__longitude = self.__longitude + 0.1 if self.__longitude < longitude else self.__longitude - 0.1
        self._dock_on()
        print(f'I am at this coordinates: latitude: {latitude}, longitude: {longitude}')

    # Encapsulation
    # Polimorphism
    def _stop_engine(self):
        self.__star_engine = False
        print('Stop Engine')

