from abc import ABC, abstractmethod


class ISwimmable(ABC):

    @abstractmethod
    def swim(self, latitude, longitude):
        pass

    @abstractmethod
    def _dock_off(self):
        pass

    @abstractmethod
    def _dock_on(self):
        pass