from abc import ABC, abstractmethod


class ITransport(ABC):

    @abstractmethod
    def _start_engine(self): ...

    @abstractmethod
    def _move(self, latitude, longitude): ...

    @abstractmethod
    def _stop_engine(self): ...

